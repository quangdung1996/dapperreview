﻿using CQRSDapper.DAL.Interfaces;
using CQRSDapper.DAL.Repositories;
using CQRSDapper.Domain.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace CQRSDapper.DAL.Repository
{
    public class CustomerContactV2Repository : BaseRepository<CustomerContactV2>, ICustomerContactV2Repository
    {
        public CustomerContactV2Repository(string connectionString) : base(connectionString)
        {
        }

        public async Task<IEnumerable<CustomerContactV2>> GetListContactAsync(string customerId)
        {
            var dynamicParameters = new DynamicParameters();
            dynamicParameters.Add("@customerId", customerId, DbType.String);
            return await this.QueryAsync(StoreProcedures.CustomerContactV2_GetListByCustomerId, dynamicParameters, commandType: CommandType.StoredProcedure);
        }

        public Task<CustomerContactV2> UpdateContactItemAsync(CustomerContactV2 item, string userName)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CQRSDapper.DAL
{
    public struct Configurations
    {
        public const string IMAGE_FOLDER = "IMAGE_FOLDER";
        public const string MEVN_FA_CONN = "MEVN_FA";
    }

    public struct TableName
    {
        public const string ProjectV2DetailTemp = "ProjectV2DetailTemp";
        public const string CustomerV2Temp = "CustomerV2Temp";
        public const string CustomerContactV2Temp = "CustomerContactV2Temp";
    }
    public struct StoreProcedures
    {
        //ProjectV2
        public const string ProjectV2_GetList = "usp_ProjectV2_GetList";
        public const string ProjectV2_GetProjectById = "usp_ProjectV2_GetById";
        public const string ProjectV2_GetListDetail = "usp_ProjectV2_GetListDetail";
        public const string ProjectV2_GetApprovalListDetail = "usp_ProjectV2_GetApprovalListDetail";
        public const string ProjectV2_ApprovalAction = "usp_ProjectV2_ApprovalAction";
        //Binh Add 05.05.2020
        public const string ProjectV2_UpdateStandardPriceOfDistributor = "usp_ProjectV2_UpdateStandardPriceOfDistributor";
        public const string ProjectV2_RequestAction = "usp_ProjectV2_RequestAction";
        public const string ProjectV2_GetMyApprovalList = "usp_ProjectV2_GetMyApprovalList";
        public const string ProjectV2_GetLocationOfUser = "usp_ProjectV2_GetLocationOfUser";
        public const string ProjectV2_UpdateAllowancePrices = "usp_ProjectV2_UpdateAllowancePrices";
        public const string ProjectV2_GetUserNameOfManager = "usp_ProjectV2_GetUserNameOfManager";
        public const string DistributorTargetV2_GetList = "usp_DistributorTargetV2_GetList";
        public const string CustomerV2_Import = "usp_CustomerV2_Import";
        public const string CustomerV2_GetList = "usp_CustomerV2_GetList";
        public const string CustomerV2_GetListAll = "usp_CustomerV2_GetListAll";
        public const string CustomerContactV2_Import = "usp_CustomerContactV2_Import";
        public const string CustomerContactV2_GetListByCustomerId = "usp_CustomerContactV2_GetListByCustomerId";
        public const string ProjectV2_Delete = "usp_ProjectV2_Delete";
        public const string ProjectV2_DeleteUserLocation = "usp_ProjectV2_DeleteUserInsideLocation";
        public const string ProjectV2_AddNewUserTeamLocation = "usp_ProjectV2_AddNewUserTeamLocation";
        public const string ProjectV2_GetManagerPermission = "usp_ProjectV2_GetManagerPermission";
        public const string ProjectV2_UpdateQuantity = "usp_ProjectV2_UpdateQuantity";
        public const string ProjectV2_UpdateLandingCostProjectDetail = "usp_ProjectV2_UpdateLandingCostProjectDetail";
        public const string ProjectV2_ItemDetaiAddedlIssues = "usp_ProjectV2_ItemDetaiAddedlIssues";
        public const string ProjectV2_UpdateQtyViaDpoId = "usp_ProjectV2_UpdateQtyViaDpoId";
        public const string ProjectV2_GetItemDetailToExport = "usp_ProjectV2_GetItemDetailToExport";
        public const string ProjectV2_CanBeDeleted = "usp_ProjectV2_CanBeDeleted";
        public const string ProjectV2_GetExportItems = "usp_ProjectV2_GetExportItems";
        //tiepnx
        public const string ProjectV2_CheckPricesOfProjectDetails = "usp_ProjectV2_CheckPricesOfProjectDetails";
        public const string ProjectV2_GetProjectItemDetailTemp = "usp_ProjectV2_GetProjectItemDetailTemp";
        public const string ProjectV2_SubmitItemDetail = "usp_ProjectV2_SubmitItemDetail";
        //update item projectV2Detail
        public const string ProjectV2_GetItemsUpdate = "usp_ProjectV2_GetItemsUpdate";
        public const string ProjectV2_UpdateProjectItemValue = "usp_ProjectV2_UpdateProjectItemValue";
        //Tiep
        public const string Discussion_Gets = "usp_Discussion_Gets";
        public const string Discussion_GetMailOfRequestersInProject = "usp_Discussion_GetMailOfRequestersInProject";
        public const string ProjectV2_VerifyProjectItems = "usp_ProjectV2_VerifyProjectItems";
    }

    public struct ConstantStatus
    {
        public const byte NotStart = 1;
        public const byte InProgress = 2;
        public const byte Synching = 3;
        public const byte Completed = 4;
        public const byte Closed = 5;
        public const byte Canceled = 6;
        public const byte Completed_Error = 7;

        public const string ProjectApprovalHistory_ChangedQty = "ChangedQty";
    }

    public struct ConstantSaleOrderExportStatus
    {
        public const byte NotSync = 0;
        public const byte SyncError = 1;
        public const byte Completed = 2;
    }

    public enum ReturnVal { OK = 1, Failed = 0, Error = -1 };

    public enum ErrorCode
    {
        ItemNotFound,
        NotAllowed,
        RowVersionChanged,
        ItemExists,
        ImageFolderNotExist,
        ItemInUse,
        ItemIsNull,
        StatusWrong,
        TypeWrong,
        Deactivated,
        UserEmailWrong,
        NotConfigEmail,
        NotEmailTemplate,
        ResetPasswordExpired
    }

    public struct ErrorText
    {
        public static Dictionary<ErrorCode, string> Texts = new Dictionary<ErrorCode, string>
        {
            { ErrorCode.ItemNotFound, "Could not find this item. It seems that you do not have enough permission or this item has been deleted." },
            { ErrorCode.NotAllowed, "You do not have enough permission." },
            { ErrorCode.RowVersionChanged, "Row version was changed." },
            { ErrorCode.ItemExists, "This item exists." },
            { ErrorCode.ImageFolderNotExist, "The folder image does not exist." },
            { ErrorCode.ItemInUse, "Could not delete this item. It has been used." },
            { ErrorCode.ItemIsNull, "The item is null." },
            { ErrorCode.StatusWrong, "The 'status' field is not configured correctly." },
            { ErrorCode.TypeWrong, "The 'type' field is not configured correctly." },
            { ErrorCode.Deactivated, "This item has deactivated." },
            { ErrorCode.UserEmailWrong, "User Name or Email is wrong." },
            { ErrorCode.NotConfigEmail, "The email settings is not configured correctly." },
            { ErrorCode.NotEmailTemplate, "The email template is not exist." },
            { ErrorCode.ResetPasswordExpired, "The key expired." },
        };
    }
    public struct StoreProcedure_Permission
    {
        public const string LoadSecurityUserPermissionsFeaturesByUserFeatureCode = "usp_SecurityUserPermissionsFeaturesByUserFeatureCode";
        public const string LoadSecurityUserPermissionsFeaturesByUserId = "usp_SecurityUserPermissionsFeaturesByUserId";
        public const string LoadSecurityGroupPermissionsFeaturesByGroupId = "usp_SecurityGroupPermissionsFeaturesByGroupId";
    }

    public struct SecurityFeatures
    {
        public const string ADMIN_FUNCTION = "ADMIN_FUNCTION";
    }

    public enum SecurityPermissionCode
    {
        View = 1,
        Add,
        Edit,
        Delete
    }
}

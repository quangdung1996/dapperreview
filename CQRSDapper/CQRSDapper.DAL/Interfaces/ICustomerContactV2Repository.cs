﻿using CQRSDapper.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CQRSDapper.DAL.Interfaces
{
    public interface ICustomerContactV2Repository
    {
        Task<IEnumerable<CustomerContactV2>> GetListContactAsync(string customerId);
        Task<CustomerContactV2> UpdateContactItemAsync(CustomerContactV2 item, string userName);
    }
}

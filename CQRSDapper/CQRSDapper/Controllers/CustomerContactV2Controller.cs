﻿using CQRSDapper.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CQRSDapper.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerContactV2Controller : ControllerBase
    {
        private readonly ICustomerContactV2Repository _repository;

        public CustomerContactV2Controller(ICustomerContactV2Repository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> GetListAsync(string customerId)
        {
            return Ok(await _repository.GetListContactAsync(customerId));
        }
    }
}
﻿using Dapper.Contrib.Extensions;
using System;

namespace CQRSDapper.Domain.Models
{
    public class CustomerContactV2
    {
        public int RowId { get; set; }

        public int CustomerId { get; set; }

        public string Email { get; set; }

        public string ContactName { get; set; }

        public string Department { get; set; }

        public string Title { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public string Address { get; set; }

        public string EducationBackground { get; set; }
        public DateTime? DateOfBirth { get; set; }

        public string Facebook { get; set; }

        public string LinkIn { get; set; }

        public string Note { get; set; }
        public DateTime? Created { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? Modified { get; set; }

        public string ModifiedBy { get; set; }
        public bool? Deleted { get; set; }
    }
}
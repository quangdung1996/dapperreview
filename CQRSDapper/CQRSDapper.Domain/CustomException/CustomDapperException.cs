﻿using System;

namespace CQRSDapper.Domain.ExceptionHandle
{
    public class CustomDapperException : Exception
    {
        public CustomDapperException()
        {
        }

        public CustomDapperException(string message)
      : base(message)
        {
        }

        public CustomDapperException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}